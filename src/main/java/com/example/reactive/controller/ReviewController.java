package com.example.reactive.controller;

import com.example.reactive.model.IDRequest;
import com.example.reactive.model.Review;
import com.example.reactive.model.ReviewRequest;
import com.example.reactive.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ReviewController {

    private final ReviewService reviewService;

    @Autowired
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("review/{reviewID}")
    public ResponseEntity<Mono<Review>> getReviewByID(@PathVariable("reviewID") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(reviewService.findReviewByID(id));
    }

    @GetMapping("reviews")
    public ResponseEntity<Flux<Review>> getAllReviews() {
        return ResponseEntity.status(HttpStatus.OK).body(reviewService.getAll());
    }
    @PostMapping("review/add")
    public ResponseEntity<Mono<Review>> addReview(@RequestBody ReviewRequest reviewRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(reviewService.addReview(reviewRequest));
    }

    @PostMapping("reviews")
    public ResponseEntity<Flux<Review>> findReviewsOfUser(@RequestBody IDRequest idRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(reviewService.findReviewsOfUser(idRequest.getId()));
    }

    @DeleteMapping("deleteAll")
    public ResponseEntity<Mono<Void>> deleteAllFromDB() {
        return ResponseEntity.status(HttpStatus.OK).body(reviewService.deleteAllFromDB());
    }
}
