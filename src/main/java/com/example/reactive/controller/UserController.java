package com.example.reactive.controller;

import com.example.reactive.model.User;
import com.example.reactive.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("user/{idUser}")
    public ResponseEntity<Mono<User>> getUserByID(@PathVariable String idUser) {
        System.out.println(idUser);
        return ResponseEntity.status(HttpStatus.OK).body(userService.findUserByID(idUser));
    }

    @GetMapping("users")
    public ResponseEntity<Flux<User>> getAllUsers() {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUsers());
    }

    @PostMapping("user/add")
    public ResponseEntity<Mono<User>> addUser(@RequestBody User user) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.addUser(user));
    }

    @GetMapping( "users/{firstName}")
    public ResponseEntity<Flux<User>> findUsersByFirstName(@PathVariable String firstName) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.findUserWhereFirstNameEquals(firstName));
    }
}
