package com.example.reactive.service;

import com.example.reactive.model.User;
import org.bson.types.ObjectId;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    public Mono<User> findUserByID(String id);

    public Flux<User> findUserWhereFirstNameEquals(String firstName);

    Mono<User> addUser(User user);

    Flux<User> getAllUsers();

}
