package com.example.reactive.service;

import com.example.reactive.model.Review;
import com.example.reactive.model.ReviewRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReviewService {

    public Mono<Review> findReviewByID(String id);

    public Flux<Review> findReviewsOfUser(String id);

    public Mono<Review> addReview(ReviewRequest reviewRequest);

    Flux<Review> getAll();

    Mono<Void> deleteAllFromDB();
}
