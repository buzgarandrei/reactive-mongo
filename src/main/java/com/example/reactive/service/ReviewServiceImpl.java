package com.example.reactive.service;

import com.example.reactive.model.Review;
import com.example.reactive.model.ReviewRequest;
import com.example.reactive.repository.ReviewRepository;
import com.example.reactive.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Service
public class ReviewServiceImpl implements ReviewService {

    private ReviewRepository reviewRepository;
    private UserRepository userRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, UserRepository userRepository) {
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Mono<Review> findReviewByID(String id) {
        return reviewRepository.findById(id);
    }

    @Override
    public Flux<Review> findReviewsOfUser(String id) {
        return reviewRepository.findReviewsByUserID(id);
    }

    @Override
    public Mono<Void> deleteAllFromDB() {

        userRepository.deleteAll().subscribe();
        reviewRepository.deleteAll().subscribe();
        return Mono.empty();
    }

    @Override
    public Mono<Review> addReview(ReviewRequest reviewRequest) {

        Review review = new Review();
        review.setReviewText(reviewRequest.getText());
        review.setNumberOfStars(reviewRequest.getNumberOfStars());
        review.setCreationTime(Instant.now());
        review.setUserID(reviewRequest.getIdUser());


        reviewRepository.save(review).share().block();
        userRepository.findById(reviewRequest.getIdUser()).
                subscribe(user -> {
                    user.getReviewList().add(review.getId());
                    userRepository.save(user).subscribe();
                });
        System.out.println(review);
        return Mono.just(review);
    }

    @Override
    public Flux<Review> getAll() {
        return reviewRepository.findAll();
    }
}
