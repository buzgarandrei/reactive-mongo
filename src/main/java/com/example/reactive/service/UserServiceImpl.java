package com.example.reactive.service;

import com.example.reactive.model.User;
import com.example.reactive.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicReference;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Mono<User> findUserByID(String id) {
        return userRepository.findById(id);
    }

    @Override
    public Flux<User> findUserWhereFirstNameEquals(String firstName) {
        return userRepository.findAllByFirstName(firstName);
    }

    @Override
    public Mono<User> addUser(User user) {
        userRepository.save(user).share().block();
        System.out.println(user.getId());
        return Mono.just(user);
    }

    @Override
    public Flux<User> getAllUsers() {
        return userRepository.findAll();
    }


}
