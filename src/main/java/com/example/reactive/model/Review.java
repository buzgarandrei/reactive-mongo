package com.example.reactive.model;

import org.springframework.data.annotation.Id;

import java.time.Instant;

public class Review {

    @Id
    private String id;
    private String userID;
    private Instant creationTime;
    private String reviewText;
    private float numberOfStars;

    @Override
    public String toString() {
        return "Review{" +
                "id='" + id + '\'' +
                ", userID='" + userID + '\'' +
                ", creationTime=" + creationTime +
                ", reviewText='" + reviewText + '\'' +
                ", numberOfStars=" + numberOfStars +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public float getNumberOfStars() {
        return numberOfStars;
    }

    public void setNumberOfStars(float numberOfStars) {
        this.numberOfStars = numberOfStars;
    }
}
